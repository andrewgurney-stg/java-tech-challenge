package com.gurneyad.tech.controller;


import com.gurneyad.tech.data.User;
import com.gurneyad.tech.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    @Test
    public void getUserById() throws Exception{
        User user = new User();
        user.setUserId(200);
        user.setUsername("test");

        given(userService.getUserById(200)).willReturn(user);

        mvc.perform(get("/users/200").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}