package com.gurneyad.tech.repository;

import com.gurneyad.tech.data.Message;
import com.gurneyad.tech.data.User;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static junit.framework.TestCase.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageRepositoryTest {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindMessage() {
        User user1 = new User();
        user1.setUsername("test1");
        user1.setActive(true);
        user1 = userRepository.save(user1);

        User user2 = new User();
        user2.setUsername("test2");
        user2.setActive(true);
        user2 = userRepository.save(user2);

        Message m1 = new Message();
        m1.setMessage("message");
        m1.setToUser(user1.getUserId());
        m1.setFromUser(user2.getUserId());
        m1 = messageRepository.save(m1);

        Optional<Message> found = messageRepository.findById(m1.getMessageId());

        assertTrue(found.isPresent());

        messageRepository.delete(m1);

        userRepository.delete(user1);
        userRepository.delete(user2);
    }
}