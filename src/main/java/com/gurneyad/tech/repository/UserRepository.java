package com.gurneyad.tech.repository;

import com.gurneyad.tech.data.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

}
