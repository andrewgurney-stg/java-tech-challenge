package com.gurneyad.tech.controller;

import com.gurneyad.tech.data.Message;
import com.gurneyad.tech.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController()
public class MessageController {

    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    MessageService messageService;

    @GetMapping("/messages")
    public Page<Message> retrieveMessages(Pageable pageRequest) {
        return messageService.retrieveMessages(pageRequest);
    }

    @GetMapping("/messages/{id}")
    public Message retrieveUser(@PathVariable Long id) {
        return messageService.retrieveUser(id);
    }

    @PostMapping("/messages")
    public ResponseEntity<Object> createMessage(@RequestBody Message messageIn) {
        try {
            Message message = messageService.saveMessage(messageIn);

            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(message.getMessageId()).toUri();

            return ResponseEntity.created(location).build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
