package com.gurneyad.tech.service;

import com.gurneyad.tech.data.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    User getUserById(Integer id);

    User saveUser(User user);

    Page<User> findAll(Pageable pageRequest);
}
