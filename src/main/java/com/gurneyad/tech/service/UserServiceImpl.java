package com.gurneyad.tech.service;

import com.gurneyad.tech.data.User;
import com.gurneyad.tech.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Override
    public User getUserById(Integer id) {
        Optional<User> user = userRepository.findById(id);

        if (!user.isPresent()) {
            logger.error("id-" + id + " could not be found in the user table");
            return null;
        }

        return user.get();
    }

    @Override
    public User saveUser(User userIn) {
        userIn.setUserId(null);
        try {
            return userRepository.save(userIn);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public Page<User> findAll(Pageable pageRequest) {
        return userRepository.findAll(pageRequest);
    }
}
