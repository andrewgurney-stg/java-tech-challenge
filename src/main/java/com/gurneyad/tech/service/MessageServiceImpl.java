package com.gurneyad.tech.service;

import com.gurneyad.tech.data.Message;
import com.gurneyad.tech.repository.MessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MessageServiceImpl implements MessageService {
    private static final Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Autowired
    MessageRepository messageRepository;

    @Override
    public Page<Message> retrieveMessages(Pageable pageRequest) {
        return messageRepository.findAll(pageRequest);
    }

    @Override
    public Message retrieveUser(Long id) {
        Optional<Message> user = messageRepository.findById(id);

        if (!user.isPresent()) {
            logger.error("id-" + id + " could not be found in the user table");
            return null;
        }

        return user.get();
    }

    @Override
    public Message saveMessage(Message messageIn) {
        messageIn.setMessageId(null);
        try {
            return messageRepository.save(messageIn);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }
}
