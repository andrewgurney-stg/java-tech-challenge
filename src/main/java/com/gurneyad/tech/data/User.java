package com.gurneyad.tech.data;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer userId;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private Boolean active;

    @OneToMany(targetEntity = Message.class, mappedBy = "toUser", cascade = CascadeType.ALL)
    private Set<Message> messages;
}
