package com.gurneyad.tech.data;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "message")
@Data
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id")
    private Long messageId;

    @Column(nullable = false)
    private String message;

    @Column(name = "user_id")
    private Integer toUser;

    @Column(name = "from_user_id")
    private Integer fromUser;
}
