package com.gurneyad.tech;

//import com.gurneyad.tech.data.Message;
//import com.gurneyad.tech.data.User;
//import com.gurneyad.tech.repository.MessageRepository;
//import com.gurneyad.tech.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//import javax.transaction.Transactional;


@EnableJpaRepositories("com.gurneyad.tech.repository")
@EntityScan("com.gurneyad.tech.data")
@SpringBootApplication
//public class TechApplication implements CommandLineRunner { //Command Line Runner is temporary
public class TechApplication {
	private static final Logger logger = LoggerFactory.getLogger(TechApplication.class);
//
//	@Autowired
//	private UserRepository userRepository;
//
//	@Autowired
//	private MessageRepository messageRepository;

	public static void main(String[] args) {
		SpringApplication.run(TechApplication.class, args);
	}

//	@Override
//	@Transactional
//	public void run(String... strings) {
//		User andrew = new User();
//		andrew.setUsername("gurneyad");
//		andrew.setActive(true);
//
//		User steve = new User();
//		steve.setUsername("steve");
//		steve.setActive(true);
//
//		userRepository.save(andrew);
//		userRepository.save(steve);
//
//		Message m1 = new Message();
//		m1.setFromUser(steve);
//		m1.setToUser(andrew);
//		m1.setMessage("Testing Testing Testing");
//
//		messageRepository.save(m1);
//
//		for (Message m : messageRepository.findAll()) {
//			logger.info(m.toString());
//		}
//	}
}
